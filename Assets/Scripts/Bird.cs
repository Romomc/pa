﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{

    public AudioClip flap;
    public AudioClip flap1;
    public AudioClip hit;
    public AudioClip score;

    public static Animator animator;
    public Text healthText;
    private int playerHealth = 50;
    public static Bird Instance;
    public int playerCurrentHealth = 10;
    private int secondsUntilNextLevel = 1;
    private GameObject levelImage;



    void Start()
    {
        animator = GetComponent<Animator>();
        /*playerHealth = Bird.Instance.playerCurrentHealth;*/
        healthText.text = "Health: " + playerHealth;
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(0.15f, 0, 0);
            animator.SetTrigger("birdfly");
            SoundController.Instance.PlaySingle(flap);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(-0.15f, 0, 0);
            animator.SetTrigger("birdfly");
            SoundController.Instance.PlaySingle(flap);
        } if (Input.GetKeyDown("space"))
        {
            transform.Translate(Vector3.up * 70 * Time.deltaTime, Space.World);
            SoundController.Instance.PlaySingle(flap, flap1);

        }
    }


    private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
    {
        if (objectPlayerCollidedWith.tag == "Hurt")
        {
            SoundController.Instance.PlaySingle(hit);
            animator.SetTrigger("birdhurt");
            
        }
        else if (objectPlayerCollidedWith.tag == "Enemy")
        {
            Debug.Log("wow");
            SoundController.Instance.PlaySingle(hit);
            playerHealth -= 1;
            animator.SetTrigger("birdhurt");
        }

        if(objectPlayerCollidedWith.tag == "Jump")
        {
            SoundController.Instance.PlaySingle(score);
            objectPlayerCollidedWith.gameObject.SetActive(false);
            transform.Translate(Vector3.up * 300 * Time.deltaTime, Space.World);
            levelImage = GameObject.FindWithTag("over");
            enabled = false;
        }
    }
}

   /* public void OnCollisionEnter2D(Collider2D collider)
    {
        if(collider.tag == "Enemy")
        {
            playerHealth -= 1;
        }
    }*/
