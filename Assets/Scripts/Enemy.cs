﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour
{
    private Transform player;
    private bool skipCurrentMove;
    public int attackdamage;
    public static Animator animator;
    public int xAxis = 0;
    public int yAxis = 0;

    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Bird").transform;
    }


    void Update()
    {
        

        float xAxisDistance = Mathf.Abs(player.position.x - transform.position.x);
        float yAxisDistance = Mathf.Abs(player.position.y - transform.position.y);

        if (xAxisDistance > yAxisDistance)
        {
            if (player.position.x > transform.position.x)
            {
                xAxis = 1;
            }
            else
            {
                xAxis = -1;
            }
        }
        else
        {
            if (player.position.y > transform.position.y)
            {
                yAxis = 1;
            }
            else
            {
                yAxis = -1;
            }
        }
    }

    private void OnTriggerEnter2d(Collider2D objectPlayerColliedWith)
    {
        if(objectPlayerColliedWith.tag == "Bird")
        {
            Debug.Log("attack");
            animator.SetTrigger("dragonAttack");
        }
    }
        
}
